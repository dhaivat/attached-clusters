variable "gke_net" {}
variable "gke_list" {}
variable "gke_location_list" {}
variable "eks_list" {}
variable "eks_ingress_ip_list" {}
variable "eks_eip_list" {}
variable "asm_version" {}
variable "project_id" {}

variable "gke_net" {
    default = "stage-gcp-vpc"
}

variable "asm_version" {
    default = "1.6.8-asm.9"
}

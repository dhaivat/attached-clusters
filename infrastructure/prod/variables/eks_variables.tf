variable "eks1_cluster_name" { default = "eks-prod-us-west2ab-1" }
variable "eks2_cluster_name" { default = "eks-prod-us-west2ab-2" }

variable "env" { default = "prod" }
